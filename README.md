# Dotfiles para instalação do i3

## Como aplicar as configurações:

Para que as alterações sejam aplicadas, as pastas presentes nesse projeto devem ser movidas/copiadas para o diretório oculto chamado ".config".
Para mais informações sobre a pasta ".config" e sobre dotfiles, leia o seguinte artigo: https://wiki.archlinux.org/index.php/Dotfiles_(Portugu%C3%AAs)

## Como tirar total proveito das configurações:

1. Essa configuração é baseada no famigerado esquema de cor dracula, logo é extremamente recomendado o seu uso nos demais aplicativos em favor da coesão e coerência visual.

2. Segue a lista dos programas/pacotes que precisam estar instalados para o funcionamento pleno da configuração:
* i3 (gerenciador de janelas);
* rofi (menu de aplicativos);
* i3status (barra de status e utilitários);
* Jetbrains Mono (fonte);
* nitrogen (responsável por definir o wallpaper);
* picom (compositor para efeitos sutis);
* firefox (navegador de internet);
* ranger para CLI e pcmanfm para GUI (gerenciadores de arquivos);

*Lembre-se, você pode sempre comentar ou deletar algum atalho*
